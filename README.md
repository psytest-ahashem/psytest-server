# PSYTEST Server
A project generated using Laravel ^9.2 and done by Abdulrahman Hashem.
## Installation

- Make sure you have, `PHP 8.1` and `Laravel 9.2` installed.

- Make sure you have `composer` installed.

- Copy environment variables file:
```shell
cp .env.example .env.
```

- Make `sqlite.database` file inside the root `database` folder.


- Install vendors via composer:
```shell
composer install
```

- Prepare the database:
```shell
php artisan migrate
```

- Seed the tests module database:
```shell
php artisan module:seed Tests
```

## Running
- Run the local server:
```shell
php artisan serve
```

## Tests
- To run the tests:
```shell
php artisan test
```
