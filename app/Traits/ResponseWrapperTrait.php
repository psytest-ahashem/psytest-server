<?php
namespace App\Traits;

trait ResponseWrapperTrait {
    public function success($data, $code = 200)
    {
        return response()->json([
            'data' => $data
        ], $code);
    }

    public function failure($message, $code = 422)
    {
        return response()->json([
            'message' => $message
        ], $code);
    }
}
