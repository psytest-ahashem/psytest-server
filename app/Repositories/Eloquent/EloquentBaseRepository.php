<?php

namespace App\Repositories\Eloquent;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EloquentBaseRepository
 *
 * @package App\Repositories\Eloquent
 */
abstract class EloquentBaseRepository implements BaseRepository
{

    /**
     * @var Model An instance of the Eloquent Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @param $function
     * @param $args
     * @return mixed
     */
    public function __call($function, $args)
    {
        $functionType = strtolower(substr($function, 0, 3));
        $propName = lcfirst(substr($function, 3));
        switch ($functionType) {
            case 'get':
                if (property_exists($this, $propName)) {
                    return $this->$propName;
                }
                break;
            case 'set':
                if (property_exists($this, $propName)) {
                    $this->$propName = $args[0];
                }
                break;
        }
    }

    public function findOrFail(int $id, ?array $with = null, ?array $order = null)
    {
        $model = $this->find($id, $with, $order);
        return $model ?? abort(404,'Not Found');
    }

    /**
     * @param int $id
     * @param array|null $with
     * @param array|null $order
     * @param array $columns
     * @return mixed
     */
    public function find(int $id, ?array $with = null, ?array $order = null, $columns = ['*']): ?Model
    {
        $query = $this->model->newQuery();

        $this->processQuery($query,$with,$order);


        return $query->find($id, $columns);
    }

    /**
     * @param array|string[] $columns
     * @param array|null $with
     * @param array $columns
     * @return Collection
     */
    public function all($columns = ['*'], ?array $with = null,$orderBy = 'id', $orderDir = 'desc'): Collection
    {
        $query = $this->model->newQuery();

        $this->processQuery($query,$with);

        return $query->orderBy($orderBy, $orderDir)->get($columns);
    }


    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function insert(array $data): bool
    {
        return $this->model->newQuery()->insert($data);
    }

    /**
     * @param array $data
     * @param array $conditions
     * @return bool
     */
    public function update(array $data, array $conditions): bool
    {
        return $this->model->where($conditions)->update($data);
    }

    /**
     * @param array $conditions
     * @param array $data
     * @return Model
     */
    public function updateOrCreate(array $conditions, array $data): Model
    {
        return $this->model->updateOrCreate($conditions, $data);
    }

    /**
     * @param array $conditions
     * @return bool|null
     */
    public function destroy(array $conditions): ?bool
    {
        return $this->model->where($conditions)->delete();
    }

    /**
     * @param array $values
     * @param string $column
     * @return bool|null
     */
    public function destroyMany(array $values,$column='id'): ?bool
    {
        return $this->model->whereIn($column,$values)->delete();
    }


    /**
     * @param array $attributes
     * @param array|null $with
     * @param array|null $order
     * @param array $columns
     * @return Model|null
     */
    public function findByAttributes(array $attributes, ?array $with = null, ?array $order = null, $columns = ['*']): ?Model
    {
        $query = $this->buildQueryByAttributes($attributes);

        $this->processQuery($query,$with,$order);

        return $query->first($columns);
    }

    /**
     * Build Query to catch resources by an array of attributes and params
     * @param array $attributes
     * @param null|string $orderBy
     * @param string $sortOrder
     * @return Builder
     */
    private function buildQueryByAttributes(array $attributes, ?string $orderBy = null, string $sortOrder = 'asc')
    {
        $query = $this->model->query();

        foreach ($attributes as $field => $value) {
            $query = $query->where($field, $value);
        }

        if (null !== $orderBy) {
            $query->orderBy($orderBy, $sortOrder);
        }

        return $query;
    }

    /**
     * @param array $attributes
     * @param array|null $with
     * @param array $columns
     * @param string|null $orderBy
     * @param string $sortOrder
     * @return Collection
     */
    public function getByAttributes(array $attributes, ?array $with = null, array $columns = ['*'], ?string $orderBy = null, string $sortOrder = 'asc'): Collection
    {
        $query = $this->buildQueryByAttributes($attributes, $orderBy, $sortOrder);

        $this->processQuery($query,$with);

        return $query->get($columns);
    }




    protected function processQuery(&$query,$with=null,$order=null){
        if ($with) {
            $query->with($with);
        }

        if (method_exists($this->model, 'locales')) {
            $query->withLocale();
        }

        if ($order && is_array($order)) {
            foreach ($order as $key => $sort) {
                $query->orderBy($key, $sort);
            }
        }
    }

    /**
     * @return bool
     */
    public function clearCache(): bool
    {
        return true;
    }
}
