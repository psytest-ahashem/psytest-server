<?php

namespace Modules\Tests\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Tests\Repositories\TestRepository;

class SubmitAnswerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $testRepository = app(TestRepository::class);
        $test = $testRepository->find(intval($this->route('testId')));

        if (empty($test)) {
            abort(404, 'No test found with the given ID');
        }

        $questionsCount = $test->questions()->count();

        return [
            'answers' => ['required', 'array', "min:$questionsCount", "max:$questionsCount"],
            'answers.*.questionId' => ['required', 'integer', 'exists:questions,id'],
            'answers.*.optionId' => ['required', 'integer', 'exists:question_options,id'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
