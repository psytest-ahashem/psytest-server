<?php

namespace Modules\Tests\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TestRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $uniqueRule = Rule::unique('tests', 'slug');
        if ($this->isMethod('patch')) {
            $uniqueRule = $uniqueRule->ignore($this->testId);
        }

        return [
            'slug' => ['required', $uniqueRule],
            'title' => ['required', 'min:5'],
            'image' => ['required'],
            'disabled' => ['boolean']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
