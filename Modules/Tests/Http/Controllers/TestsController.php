<?php

namespace Modules\Tests\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Tests\Http\Requests\SubmitAnswerRequest;
use Modules\Tests\Http\Requests\TestRequest;
use Modules\Tests\Services\TestAnswerService;
use Modules\Tests\Services\TestService;
use Modules\Tests\Transformers\TestRefResource;
use Modules\Tests\Transformers\TestSubmissionResource;

class TestsController extends Controller
{
    private TestService $testService;
    private TestAnswerService $testAnswerService;

    public function __construct(TestService       $testService,
                                TestAnswerService $testAnswerService)
    {
        $this->testService = $testService;
        $this->testAnswerService = $testAnswerService;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->success($this->testService->getAllTestRefs());
    }

    /**
     * Store a newly created resource in storage.
     * @param TestRequest $request
     * @return JsonResponse
     */
    public function store(TestRequest $request): JsonResponse
    {
        $test = $this->testService->save($request->only(['slug', 'title', 'image', 'disabled']));
        return $this->success(new TestRefResource($test), 201);
    }

    /**
     * Show the specified resource.
     * @param string $slug
     * @return JsonResponse
     */
    public function show(string $slug): JsonResponse
    {
        return $this->success($this->testService->getTestDetails($slug));
    }

    /**
     * Update the specified resource in storage.
     * @param TestRequest $request
     * @param int $testId
     * @return JsonResponse
     */
    public function update(TestRequest $request, int $testId): JsonResponse
    {
        return $this->success(new TestRefResource($this->testService
                ->update($testId, $request->only(['title', 'slug', 'image', 'disabled'])))
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $testId
     * @return JsonResponse
     */
    public function destroy(int $testId): JsonResponse
    {
        $this->testService->delete($testId);
        return $this->success([], 202);
    }

    /**
     * @param SubmitAnswerRequest $request
     * @param Integer $testId
     * @return JsonResponse
     */
    public function submitAnswer(SubmitAnswerRequest $request, int $testId): JsonResponse
    {
        $token = $this->testAnswerService->submitAnswer($testId, $request->get('answers'));
        return $this->success(['token' => $token], 201);
    }

    /**
     * @param string $slug
     * @param string $token
     * @return JsonResponse
     */
    public function getResult(string $slug, string $token): JsonResponse
    {
        return $this->success(new TestSubmissionResource($this->testService->getResult($slug, $token)));
    }
}
