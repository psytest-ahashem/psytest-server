<?php

namespace Modules\Tests\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\Tests\Http\Requests\StoreQuestionRequest;
use Modules\Tests\Services\QuestionService;
use Modules\Tests\Transformers\QuestionResource;

class QuestionsController extends Controller
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
    }

    /**
     * Display a listing of the resource.
     * @param int $testId
     * @return JsonResponse
     */
    public function index(int $testId): JsonResponse
    {
        return $this->success(QuestionResource::collection($this->questionService->findByTestId($testId)));
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreQuestionRequest $request
     * @return JsonResponse
     */
    public function store(StoreQuestionRequest $request): JsonResponse
    {
        return $this->success(new QuestionResource($this->questionService
            ->create($request->only(['title', 'options']))));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->success(new QuestionResource($this->questionService->findById($id)));
    }

    /**
     * Update the specified resource in storage.
     * @param StoreQuestionRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(StoreQuestionRequest $request, int $id): JsonResponse
    {
        return $this->success(
            new QuestionResource($this->questionService->update($id, $request->only(['title', 'options'])))
        );
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->questionService->delete($id);
        return $this->success([], 204);
    }
}
