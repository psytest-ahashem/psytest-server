<?php

namespace Modules\Tests\Providers;

use Carbon\Laravel\ServiceProvider;
use Modules\Tests\Entities\Question;
use Modules\Tests\Entities\QuestionOption;
use Modules\Tests\Entities\Test;
use Modules\Tests\Entities\TestAnswer;
use Modules\Tests\Entities\TestOptionScore;
use Modules\Tests\Repositories\Eloquent\EloquentQuestionOptionRepository;
use Modules\Tests\Repositories\Eloquent\EloquentQuestionRepository;
use Modules\Tests\Repositories\Eloquent\EloquentTestAnswerRepository;
use Modules\Tests\Repositories\Eloquent\EloquentTestOptionScoreRepository;
use Modules\Tests\Repositories\Eloquent\EloquentTestRepository;
use Modules\Tests\Repositories\QuestionOptionRepository;
use Modules\Tests\Repositories\QuestionRepository;
use Modules\Tests\Repositories\TestAnswerRepository;
use Modules\Tests\Repositories\TestOptionScoreRepository;
use Modules\Tests\Repositories\TestRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(TestRepository::class, function () {
            return new EloquentTestRepository(new Test());
        });
        $this->app->bind(QuestionRepository::class, function () {
            return new EloquentQuestionRepository(new Question());
        });
        $this->app->bind(QuestionOptionRepository::class, function () {
            return new EloquentQuestionOptionRepository(new QuestionOption());
        });
        $this->app->bind(TestAnswerRepository::class, function () {
            return new EloquentTestAnswerRepository(new TestAnswer());
        });
        $this->app->bind(TestOptionScoreRepository::class, function () {
            return new EloquentTestOptionScoreRepository(new TestOptionScore());
        });
    }
}
