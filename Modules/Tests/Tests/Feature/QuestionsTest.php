<?php

namespace Modules\Tests\Tests\Feature;

use Tests\TestCase;

class QuestionsTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldListTestQuestions()
    {
        $response = $this->get(route('tests-questions.list', ['testId' => 1]));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => ['*' => [
                    'id',
                    'title',
                    'options' => ['*' => [
                        'id',
                        'text',
                    ]]
                ]]
            ]);
    }


    /**
     * @test
     */
    public function itShouldGetQuestionDetails()
    {
        $response = $this->get(route('questions.show', ['question' => 1]));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'options' => ['*' => [
                        'id',
                        'order',
                        'text',
                    ]]
                ]
            ]);
    }


    /**
     * @test
     */
    public function itShouldStoreQuestionWithItsOptions()
    {
        $options = [
            ['text' => 'new option 1', 'order' => '1'],
            ['text' => 'new option 2', 'order' => '2'],
        ];

        $response = $this->post(route('questions.store'), [
            'title' => 'new question',
            'options' => $options
        ]);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'options' => ['*' => [
                        'id',
                        'order',
                        'text',
                    ]]
                ]
            ])->assertJsonFragment(['title' => 'new question']);
    }


    /**
     * @test
     */
    public function itShouldDeleteQuestion()
    {
        $options = [
            ['text' => 'new option 1', 'order' => '1'],
            ['text' => 'new option 2', 'order' => '2'],
        ];

        $response = $this->post(route('questions.store'), [
            'title' => 'new question',
            'options' => $options
        ]);

        $response = $this->delete(route('questions.destroy', ['question' => $response->decodeResponseJson()['data']['id']]));

        $response->assertStatus(204);
    }


    /**
     * @test
     */
    public function itShouldThrow404WhenQuestionIsNotFound()
    {
        $response = $this->delete(route('questions.destroy', ['question' => 121212]));

        $response->assertStatus(404);
    }
}
