<?php

namespace Modules\Tests\Tests\Feature;

use Modules\Tests\Entities\TestSubmission;
use Tests\TestCase;

class TestsTest extends TestCase
{
    /**
     * @test
     */
    public function itShouldListTests()
    {
        $response = $this->get(route('tests.list'));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => ['*' => [
                    'id',
                    'slug',
                    'title',
                    'image',
                    'disabled'
                ]]
            ])->assertJsonFragment(['slug' => 'are-you-an-introvert-or-an-extrovert']);
    }

    /**
     * @test
     */
    public function itShouldGetTestDetails()
    {
        $response = $this->get(route('tests.get', [
            'slug' => 'are-you-an-introvert-or-an-extrovert'
        ]));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'slug',
                    'title',
                    'image',
                    'disabled',
                    'questions' => ['*' => [
                        'id',
                        'title',
                        'order',
                        'options' => ['*' => [
                            'id',
                            'order',
                            'text'
                        ]]
                    ]]
                ]
            ])->assertJsonFragment(['slug' => 'are-you-an-introvert-or-an-extrovert']);
    }

    /**
     * @test
     */
    public function itShouldShowTestResults()
    {
        $submission = TestSubmission::query()->first();
        if (!$submission) {
            return;
        }

        $response = $this->get(route('tests.results', [
            'slug' => 'are-you-an-introvert-or-an-extrovert',
            'token' => $submission->token
        ]));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'test',
                    'heading',
                    'text',
                    'answers',
                ]
            ])->assertJsonFragment(['slug' => 'are-you-an-introvert-or-an-extrovert']);
    }

    /**
     * @test
     */
    public function itShouldNotShowTestResultsWhenSlugDoesNotExist()
    {
        $response = $this->get(route('tests.results', [
            'slug' => 'not-found-slug',
            'token' => 'gkiP4dhoHPWvwoFJPuNbZZdLcGsOBRwU'
        ]));

        $response->assertStatus(404)
            ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function itShouldNotShowTestResultsWhenTokenDoesNotExist()
    {
        $response = $this->get(route('tests.results', [
            'slug' => 'are-you-an-introvert-or-an-extrovert',
            'token' => 'token-which-does-not-exist'
        ]));

        $response->assertStatus(404)
            ->assertJsonStructure(['message']);
    }

    /**
     * @test
     */
    public function itShouldSubmitTest()
    {
        $response = $this->post(route('tests.submit', ['testId' => 1]), [
            'answers' => [
                ['questionId' => 1, 'optionId' => 1],
                ['questionId' => 2, 'optionId' => 2],
                ['questionId' => 3, 'optionId' => 3],
                ['questionId' => 4, 'optionId' => 4],
                ['questionId' => 5, 'optionId' => 5],
            ]
        ]);
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => ['token']
            ]);
    }
}
