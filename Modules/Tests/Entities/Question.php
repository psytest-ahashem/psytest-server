<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'title'
    ];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\QuestionFactory::new();
    }

    public function tests()
    {
        return $this->belongsToMany(Test::class, 'test_questions');
    }

    public function options()
    {
        return $this->hasMany(QuestionOption::class);
    }
}
