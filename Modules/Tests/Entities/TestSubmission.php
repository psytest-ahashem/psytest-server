<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TestSubmission extends Model
{
    use HasFactory;

    protected $fillable = [
        'token',
        'score'
    ];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\TestSubmissionFactory::new();
    }

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class);
    }

    public function answers()
    {
        return $this->hasMany(TestAnswer::class, 'submission_id');
    }
}
