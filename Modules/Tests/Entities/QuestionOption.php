<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class QuestionOption extends Model
{
    use HasFactory;

    protected $fillable = [
        'order',
        'text'
    ];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\QuestionOptionFactory::new();
    }

    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    public function scores(): HasMany
    {
        return $this->hasMany(TestOptionScore::class);
    }
}
