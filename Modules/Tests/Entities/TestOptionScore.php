<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TestOptionScore extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\TestOptionScoreFactory::new();
    }

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function option()
    {
        return $this->belongsTo(QuestionOption::class);
    }
}
