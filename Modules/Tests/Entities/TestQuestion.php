<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TestQuestion extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\TestQuestionFactory::new();
    }

    public function options(): HasMany
    {
        return $this->hasMany(QuestionOption::class);
    }
}
