<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TestRange extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\TestRangeFactory::new();
    }

    public function test(): BelongsTo
    {
        return $this->belongsTo(Test::class);
    }
}
