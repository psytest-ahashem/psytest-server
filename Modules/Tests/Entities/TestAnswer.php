<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestAnswer extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\TestSubmissionFactory::new();
    }

    public function submission()
    {
        return $this->belongsTo(TestSubmission::class);
    }

    public function option()
    {
        return $this->belongsTo(QuestionOption::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
