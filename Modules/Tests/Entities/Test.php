<?php

namespace Modules\Tests\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Test extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'title',
        'image',
        'disabled'
    ];

    protected $casts = [
        'disabled' => 'boolean'
    ];

    protected static function newFactory()
    {
        return \Modules\Tests\Database\factories\TestFactory::new();
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'test_questions')
            ->withPivot('order');
    }

    public function submissions(): HasMany
    {
        return $this->hasMany(TestSubmission::class);
    }

    public function ranges(): HasMany
    {
        return $this->hasMany(TestRange::class);
    }

    public function scores(): HasMany
    {
        return $this->hasMany(TestOptionScore::class);
    }
}
