<?php

namespace Modules\Tests\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge([
            'id' => $this->id,
            'title' => $this->title,
            'options' => QuestionOptionResource::collection($this->options)
        ], empty($this->pivot) ? [] : ['order' => $this->pivot->order]);
    }
}
