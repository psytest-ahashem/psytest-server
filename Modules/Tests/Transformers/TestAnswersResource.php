<?php

namespace Modules\Tests\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TestAnswersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'questionId' => $this->question_id,
            'optionId' => $this->option_id,
        ];
    }
}
