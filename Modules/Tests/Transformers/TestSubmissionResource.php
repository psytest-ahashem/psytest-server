<?php

namespace Modules\Tests\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TestSubmissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $test = new TestDetailsResource($this->test);
        $answers = TestAnswersResource::collection($this->answers);;
        $score = $this->score;
        $range = $test->ranges
            ->first(function ($range) use($score) {
                return $score >= $range->start && $score <= $range->end;
            });

        return [
            'test' => $test,
            'text' => $range->text,
            'heading' => $range->heading,
            'answers' => $answers,
        ];
    }
}
