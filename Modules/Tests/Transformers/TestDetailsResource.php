<?php

namespace Modules\Tests\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TestDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $testRef = new TestRefResource($this);
        return array_merge(
            $testRef->toArray($request),
            ['questions' => QuestionResource::collection($this->questions)],
        );
    }
}
