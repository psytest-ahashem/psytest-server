<?php

use Illuminate\Support\Facades\Route;
use Modules\Tests\Http\Controllers\QuestionsController;
use Modules\Tests\Http\Controllers\TestsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('questions', QuestionsController::class)->except('index');

Route::prefix('tests')->group(function () {
    Route::get('', [TestsController::class, 'index'])->name('tests.list');
    Route::post('', [TestsController::class, 'store'])->name('tests.post');
    Route::patch('', [TestsController::class, 'update'])->name('tests.patch');

    Route::prefix('{testId}')->group(function () {
        Route::get('questions', [QuestionsController::class, 'index'])->name('tests-questions.list');
        Route::post('answers', [TestsController::class, 'submitAnswer'])->name('tests.submit');
        Route::delete('', [TestsController::class, 'destroy'])->name('tests.delete');
        Route::patch('', [TestsController::class, 'update'])->name('tests.patch');
    });

    Route::prefix('{slug}')->group(function () {
        Route::get('/results/{token}', [TestsController::class, 'getResult'])->name('tests.results');
        Route::get('', [TestsController::class, 'show'])->name('tests.get');
    });
});
