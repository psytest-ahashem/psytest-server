<?php

namespace Modules\Tests\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Tests\Repositories\TestRepository;
use Modules\Tests\Transformers\TestDetailsResource;
use Modules\Tests\Transformers\TestRefResource;

class TestService
{
    private TestRepository $testRepository;

    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    public function getAllTestRefs(): AnonymousResourceCollection
    {
        return TestRefResource::collection($this->testRepository->all(['*'], null, 'id', 'asc'));
    }

    public function getTestDetails(string $slug): TestDetailsResource
    {
        $test = $this->testRepository
            ->findByAttributes(['slug' => $slug], ['questions' => function ($q) {
                return $q->orderBy('order');
            }, 'questions.options']);

        if (empty($test)) {
            abort(404, 'No test found with the given URL');
        }

        return new TestDetailsResource($test);
    }

    public function getResult(string $slug, string $token): Model|Builder|null
    {
        $submission = $this->testRepository->getResultBySlugAndToken($slug, $token);

        if (empty($submission) || empty($submission->test)) {
            abort(404, 'No submission found with the provided data');
        }

        return $submission;
    }

    public function save(array $data): Model
    {
        return $this->testRepository->create($data);
    }

    public function delete(int $id): void
    {
        $test = $this->testRepository->findOrFail($id);
        $test->delete();
    }

    public function update(int $testId, array $data): ?Model
    {
        $test = $this->testRepository->findOrFail($testId);
        $test->title = $data['title'];
        $test->disabled = $data['disabled'];
        $test->slug = $data['slug'];
        $test->image = $data['image'];
        $test->save();
        return $test;
    }
}
