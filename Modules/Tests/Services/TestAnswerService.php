<?php

namespace Modules\Tests\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Modules\Tests\Entities\TestSubmission;
use Modules\Tests\Repositories\TestAnswerRepository;
use Modules\Tests\Repositories\TestOptionScoreRepository;
use Modules\Tests\Repositories\TestRepository;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class TestAnswerService
{
    private TestRepository $testRepository;
    private TestAnswerRepository $testAnswerRepository;
    private TestOptionScoreRepository $optionScoreRepository;

    public function __construct(TestRepository $testRepository,
                                TestAnswerRepository $testAnswerRepository,
                                TestOptionScoreRepository $optionScoreRepository)
    {
        $this->testRepository = $testRepository;
        $this->testAnswerRepository = $testAnswerRepository;
        $this->optionScoreRepository = $optionScoreRepository;

    }

    public function submitAnswer(int $testId, array $answersPayload)
    {
        $test = $this->testRepository->findOrFail($testId);
        try {
            DB::beginTransaction();
            $score = $this->calcSubmissionScore($testId, $answersPayload);
            $submission = new TestSubmission([
                'token' => Str::random(32),
                'score' => $score
            ]);
            $test->submissions()->save($submission);

            $answers = [];
            foreach ($answersPayload as $answer) {
                $answers[] = [
                    'submission_id' => $submission->id,
                    'question_id' => $answer['questionId'],
                    'option_id' => $answer['optionId'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }

            $done = $this->testAnswerRepository->insert($answers);
            if ($done) {
                DB::commit();
                return $submission->token;
            } else {
                DB::rollBack();
                throw new UnprocessableEntityHttpException();
            }
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            throw new UnprocessableEntityHttpException();
        }
    }

    private function calcSubmissionScore(int $testId, array $answers): int
    {
        return $this->optionScoreRepository
            ->calcScoreForOptions($testId, collect($answers)
                ->map(function ($answer) { return $answer['optionId']; }));
    }
}
