<?php

namespace Modules\Tests\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Tests\Repositories\QuestionRepository;
use Modules\Tests\Repositories\TestRepository;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class QuestionService
{
    private QuestionRepository $questionRepository;
    private TestRepository $testRepository;

    public function __construct(QuestionRepository $questionRepository,
                                TestRepository     $testRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->testRepository = $testRepository;
    }

    public function findByTestId(int $testId)
    {
        $test = $this->testRepository->findOrFail($testId, ['questions', 'questions.options']);
        return $test->questions;
    }

    public function findById(int $id): ?Model
    {
        return $this->questionRepository->findOrFail($id, ['options']);
    }

    public function delete(int $id): void
    {
        $question = $this->questionRepository->findOrFail($id);
        $question->delete();
    }

    public function create(array $data): Model
    {
        try {
            DB::beginTransaction();
            $question = $this->questionRepository->create(['title' => $data['title']]);
            $this->saveOptions($question, $data['options']);
            DB::commit();
            return $question;
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            throw new UnprocessableEntityHttpException();
        }
    }

    public function update(int $id, array $data): ?Model
    {
        try {
            DB::beginTransaction();
            $question = $this->questionRepository->findOrFail($id);
            $question->title = $data['title'];
            $question->save();
            $question->options()->delete();
            $this->saveOptions($question, $data['options']);
            DB::commit();
            return $question;
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            throw new UnprocessableEntityHttpException();
        }
    }

    /**
     * @param Model $question
     * @param array $options
     * @return void
     */
    private function saveOptions(Model $question, array $options): void
    {
        foreach ($options as $option) {
            $question->options()->create($option);
        }
    }

}
