<?php


namespace Modules\Tests\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface QuestionRepository extends BaseRepository
{
    public function createTestOption(array $data): Model|Builder;
}
