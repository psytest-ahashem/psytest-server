<?php

namespace Modules\Tests\Repositories\Eloquent;

use App\Repositories\Eloquent\EloquentBaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Tests\Entities\TestQuestion;
use Modules\Tests\Repositories\QuestionRepository;

class EloquentQuestionRepository extends EloquentBaseRepository implements QuestionRepository
{
    public function createTestOption(array $data): Model|Builder
    {
        return TestQuestion::query()->create($data);
    }
}
