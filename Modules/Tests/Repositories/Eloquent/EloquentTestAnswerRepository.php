<?php

namespace Modules\Tests\Repositories\Eloquent;

use App\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Tests\Repositories\TestAnswerRepository;

class EloquentTestAnswerRepository extends EloquentBaseRepository implements TestAnswerRepository
{
}
