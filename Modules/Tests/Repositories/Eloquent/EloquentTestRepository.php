<?php

namespace Modules\Tests\Repositories\Eloquent;

use App\Repositories\Eloquent\EloquentBaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Tests\Entities\TestSubmission;
use Modules\Tests\Repositories\TestRepository;

class EloquentTestRepository extends EloquentBaseRepository implements TestRepository
{
    public function getResultBySlugAndToken(string $slug, string $token): Model|Builder|null
    {
        return TestSubmission::query()
            ->where('token', $token)
            ->with(['test' => function ($q) use ($slug) {
                $q->where('tests.slug', $slug);
            }, 'answers', 'test.ranges', 'test.scores'])
            ->first();
    }
}
