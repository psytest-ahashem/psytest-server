<?php

namespace Modules\Tests\Repositories\Eloquent;

use App\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Tests\Repositories\QuestionOptionRepository;

class EloquentQuestionOptionRepository extends EloquentBaseRepository implements QuestionOptionRepository
{
}
