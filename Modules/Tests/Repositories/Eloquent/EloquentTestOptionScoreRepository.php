<?php

namespace Modules\Tests\Repositories\Eloquent;

use App\Repositories\Eloquent\EloquentBaseRepository;
use Illuminate\Support\Collection;
use Modules\Tests\Repositories\TestOptionScoreRepository;

class EloquentTestOptionScoreRepository extends EloquentBaseRepository implements TestOptionScoreRepository
{
    public function calcScoreForOptions(int $testId, array|Collection $optionIds): int
    {
        return $this->model->newQuery()
            ->where('test_id', $testId)
            ->whereIn('option_id', $optionIds)
            ->sum('score');
    }
}
