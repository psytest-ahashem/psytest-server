<?php


namespace Modules\Tests\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

interface TestOptionScoreRepository extends BaseRepository
{
    public function calcScoreForOptions(int $testId, array|Collection $optionIds): int;
}
