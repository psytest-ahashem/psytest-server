<?php


namespace Modules\Tests\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface TestRepository extends BaseRepository
{
    public function getResultBySlugAndToken(string $slug, string $token): Model|Builder|null;
}
