<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_option_scores', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('test_id');
            $table->unsignedInteger('option_id');

            $table->foreign('test_id')
                ->references('id')
                ->on('tests')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('option_id')
                ->references('id')
                ->on('question_options')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->integer('score');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_option_scores');
    }
};
