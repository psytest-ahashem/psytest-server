<?php

namespace Modules\Tests\Database\seeders;

use Illuminate\Database\Seeder;
use Modules\Tests\Repositories\TestRepository;

class TestScoresTableSeeder extends Seeder
{
    private TestRepository $testRepository;

    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $scores = [
            ['id' => 1, 'test_id' => 1, 'option_id' => 1, 'score' => 4],
            ['id' => 2, 'test_id' => 1, 'option_id' => 2, 'score' => 7],
            ['id' => 3, 'test_id' => 1, 'option_id' => 3, 'score' => 15],
            ['id' => 4, 'test_id' => 1, 'option_id' => 4, 'score' => 20],

            ['id' => 5, 'test_id' => 1, 'option_id' => 5, 'score' => 4],
            ['id' => 6, 'test_id' => 1, 'option_id' => 6, 'score' => 7],
            ['id' => 7, 'test_id' => 1, 'option_id' => 7, 'score' => 15],
            ['id' => 8, 'test_id' => 1, 'option_id' => 8, 'score' => 20],

            ['id' => 9, 'test_id' => 1, 'option_id' => 9, 'score' => 4],
            ['id' => 10, 'test_id' => 1, 'option_id' => 10, 'score' => 7],
            ['id' => 11, 'test_id' => 1, 'option_id' => 11, 'score' => 15],
            ['id' => 12, 'test_id' => 1, 'option_id' => 12, 'score' => 20],

            ['id' => 13, 'test_id' => 1, 'option_id' => 13, 'score' => 4],
            ['id' => 14, 'test_id' => 1, 'option_id' => 14, 'score' => 7],
            ['id' => 15, 'test_id' => 1, 'option_id' => 15, 'score' => 15],
            ['id' => 16, 'test_id' => 1, 'option_id' => 16, 'score' => 20],


            ['id' => 17, 'test_id' => 1, 'option_id' => 17, 'score' => 4],
            ['id' => 18, 'test_id' => 1, 'option_id' => 18, 'score' => 7],
            ['id' => 19, 'test_id' => 1, 'option_id' => 19, 'score' => 15],
            ['id' => 20, 'test_id' => 1, 'option_id' => 20, 'score' => 20],
        ];

        $test = $this->testRepository->find(1);

        foreach ($scores as $score) {
            $test->scores()->create($score);
        }
    }
}
