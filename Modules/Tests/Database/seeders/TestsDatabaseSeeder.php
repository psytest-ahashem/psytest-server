<?php

namespace Modules\Tests\Database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TestsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Model::unguard();
        $this->call(TestsTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(TestQuestionsTableSeeder::class);
        $this->call(TestRangesTableSeeder::class);
        $this->call(TestScoresTableSeeder::class);
        Model::reguard();
    }
}
