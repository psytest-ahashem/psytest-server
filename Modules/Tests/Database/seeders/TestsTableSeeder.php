<?php

namespace Modules\Tests\Database\seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\URL;
use Modules\Tests\Repositories\TestRepository;

class TestsTableSeeder extends Seeder
{
    private TestRepository $testRepository;

    public function __construct(TestRepository $testRepository)
    {
        $this->testRepository = $testRepository;
    }

    /**
     * Run the database seeds.
     * @return void
     */
    public function run(): void
    {
        $tests = [
            [
                'id' => 1,
                'slug' => 'are-you-an-introvert-or-an-extrovert',
                'title' => 'Are you an introvert or an extrovert?',
                'image' => url('images/introvert-extrovert.webp'),
                'disabled' => false,
            ],
            [
                'id' => 2,
                'slug' => 'what-helps-you-focus',
                'title' => 'What helps you find focus?',
                'image' => url('images/focus.webp'),
                'disabled' => true,
            ],
            [
                'id' => 3,
                'slug' => 'whats-your-natural-leadership-style',
                'title' => 'What\'s your natural leadership style?',
                'image' => url('images/leadership.jpeg'),
                'disabled' => true,
            ],
            [
                'id' => 4,
                'slug' => 'are-you-ready-for-a-new-relationship',
                'title' => 'Are you ready for a new relationship?',
                'image' => url('images/new-relationship.jpeg'),
                'disabled' => true,
            ],
        ];

        foreach ($tests as $test) {
            $this->testRepository->create($test);
        }
    }
}
