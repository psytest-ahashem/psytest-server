<?php

namespace Modules\Tests\Database\seeders;

use Illuminate\Database\Seeder;
use Modules\Tests\Repositories\QuestionRepository;

class TestQuestionsTableSeeder extends Seeder
{
    private QuestionRepository $questionRepository;

    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $testQuestions = [
            [
                'id' => 1,
                'test_id' => 1,
                'question_id' => 1,
                'order' => 1
            ],
            [
                'id' => 2,
                'test_id' => 1,
                'question_id' => 2,
                'order' => 2
            ],
            [
                'id' => 3,
                'test_id' => 1,
                'question_id' => 3,
                'order' => 3
            ],
            [
                'id' => 4,
                'test_id' => 1,
                'question_id' => 4,
                'order' => 4
            ],
            [
                'id' => 5,
                'test_id' => 1,
                'question_id' => 5,
                'order' => 5
            ],
        ];


        foreach ($testQuestions as $testQuestion) {
            $this->questionRepository->createTestOption($testQuestion);
        }
    }
}
