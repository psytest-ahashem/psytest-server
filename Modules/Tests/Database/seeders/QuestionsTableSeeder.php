<?php

namespace Modules\Tests\Database\seeders;

use Illuminate\Database\Seeder;
use Modules\Tests\Repositories\QuestionOptionRepository;
use Modules\Tests\Repositories\QuestionRepository;

class QuestionsTableSeeder extends Seeder
{
    private QuestionRepository $questionRepository;
    private QuestionOptionRepository $questionOptionRepository;

    public function __construct(QuestionRepository $questionRepository,
                                QuestionOptionRepository $questionOptionRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->questionOptionRepository = $questionOptionRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $questions = [
            [
                "id" => 1,
                "title" => "You’re really busy at work and a colleague is telling you their life story and personal woes. You:",
                "options" => [
                    [
                        "id" => 1,
                        "text" => "Don't dare to interrupt them",
                        "order" => 1
                    ],
                    [
                        "id" => 2,
                        "text" => "Think it’s more important to give them some of your time; work can wait",
                        "order" => 2
                    ],
                    [
                        "id" => 3,
                        "text" => "Listen, but with only with half an ear",
                        "order" => 3
                    ],
                    [
                        "id" => 4,
                        "text" => "Interrupt and explain that you are really busy at the moment",
                        "order" => 4
                    ]
                ]
            ],
            [
                "id" => 2,
                "title" => "You’ve been sitting in the doctor’s waiting room for more than 25 minutes. You:",
                "options" => [
                    [
                        "id" => 5,
                        "text" => "Look at your watch every two minutes",
                        "order" => 1
                    ],
                    [
                        "id" => 6,
                        "text" => "Bubble with inner anger, but keep quiet",
                        "order" => 2
                    ],
                    [
                        "id" => 7,
                        "text" => "Explain to other equally impatient people in the room that the doctor is always running late",
                        "order" => 3
                    ],
                    [
                        "id" => 8,
                        "text" => "Complain in a loud voice, while tapping your foot impatiently",
                        "order" => 4
                    ]
                ]
            ],
            [
                "id" => 3,
                "title" => "You’re having an animated discussion with a colleague regarding a project that you’re in charge of. You:",
                "options" => [
                    [
                        "id" => 9,
                        "text" => "Don’t dare contradict them",
                        "order" => 1
                    ],
                    [
                        "id" => 10,
                        "text" => "Think that they are obviously right",
                        "order" => 2
                    ],
                    [
                        "id" => 11,
                        "text" => "Defend your own point of view, tooth and nail",
                        "order" => 3
                    ],
                    [
                        "id" => 12,
                        "text" => "Continuously interrupt your colleague",
                        "order" => 4
                    ]
                ]
            ],
            [
                "id" => 4,
                "title" => "You are taking part in a guided tour of a museum. You:",
                "options" => [
                    [
                        "id" => 13,
                        "text" => "Are a bit too far towards the back so don’t really hear what the guide is saying",
                        "order" => 1
                    ],
                    [
                        "id" => 14,
                        "text" => "Follow the group without question",
                        "order" => 2
                    ],
                    [
                        "id" => 15,
                        "text" => "Make sure that everyone is able to hear properly",
                        "order" => 3
                    ],
                    [
                        "id" => 16,
                        "text" => "Are right up the front, adding your own comments in a loud voice",
                        "order" => 4
                    ]
                ]
            ],
            [
                "id" => 5,
                "title" => "During dinner parties at your home, you have a hard time with people who:",
                "options" => [
                    [
                        "id" => 17,
                        "text" => "Ask you to tell a story in front of everyone else",
                        "order" => 1
                    ],
                    [
                        "id" => 18,
                        "text" => "Talk privately between themselves",
                        "order" => 2
                    ],
                    [
                        "id" => 19,
                        "text" => "Hang around you all evening",
                        "order" => 3
                    ],
                    [
                        "id" => 20,
                        "text" => "Always drag the conversation back to themselves",
                        "order" => 4
                    ]
                ]
            ]
        ];

        foreach ($questions as $question) {
            $addedQuestion = $this->questionRepository->create([
                'id' => $question['id'],
                'title' => $question['title']
            ]);
            foreach ($question['options'] as $option) {
                $this->questionOptionRepository->create([
                    'id' => $option['id'],
                    'question_id' => $addedQuestion->id,
                    'text' => $option['text'],
                    'order' => $option['order'],
                ]);
            }
        }
    }
}
